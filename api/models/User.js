var User = {
  // Enforce model schema in the case of schemaless databases
  schema: true,

  attributes: {
    username  : { type: 'string', unique: true },
    email     : { type: 'email',  unique: true },
    isAdmin     : { type: 'boolean',  defaultTo: false},
    fio     : { type: 'string'},
    passports : { collection: 'Passport', via: 'user' },
    roles: {
      collection:'Roles',
      via:'users'
    }
  }
};

module.exports = User;
