/**
 * Carousel.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    brand:{
      type:'string'
    },
    text:{
      type:'text'
    },
    image:{
      type:'url',
      required:true
    }


  }
};

