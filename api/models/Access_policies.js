/**
 * Access_policies.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    roles:{
      collection:'Roles',
      via:'access_policies'
    },
    controller:{
      type:'string',
      required: true
    },
    actions:{
      type:'array'
    }
  }
};

