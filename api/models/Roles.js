/**
 * Roles.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name:{
      type:'string'
    },
    access_policies:{
      collection:'Access_policies',
      via:'roles',
      dominant: true
    },
    users:{
      collection:'User',
      via:'roles',
      dominant: true
    }
  }
};

