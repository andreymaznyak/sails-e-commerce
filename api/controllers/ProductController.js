/**
 * ProductController
 *
 * @description :: Server-side logic for managing Products
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var oneC77 = require('./oneC77/oneC77Adapter.js');
module.exports = {
  //
  get_root: function(req, res){
    //Все элементы корня имеют "PARENTID": "     0   "
    oneC77.get_subitems("Справочник Товары","     0   ",function(err, result){
      if(err)
        return res.end(err);
      res.json(result);
    });
  },
  get_subitems: function(req, res){
    var parent_id = req.param('parentid');
    oneC77.get_subitems("Справочник Товары",parent_id, function(err, result){
      if(err)
        return res.end(err);
      res.json(result);
    });
  },
  //
  create: notPermitted,
  find: function(req,res){
    User.find().exec(function(err, users){
      res.json(users);
    });
  },
  findone: notPermitted,
  update: notPermitted,
  destroy: notPermitted,
  populate: notPermitted,
  add: notPermitted,
  remove: notPermitted
};

function notPermitted(req, res){
  res.forbidden('You are not permitted to perform user action.');
}
