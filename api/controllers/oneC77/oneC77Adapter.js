/**
 * Created by AndreyMaznyak on 04.05.2016.
 */
'use strict';
var test_objects = require('./товары.json');
var schema1c = require('./schema1c');
module.exports = {
  get_subitems:function(table, parent_id, callback){
    if(table == "Справочник Товары"){
      console.log(parent_id);
      var filter_options = {PARENTID: parent_id},
          result = _.filter(test_objects, filter_options),
          output = [];
      var metadata_filter_options = {title: "Справочник Товары"},
          metadata_result = _.find(schema1c, metadata_filter_options);

      //console.log(metadata_result);
      for(var i = 0; i < result.length; i++){
        var output_obj = {subitems:[]};
        for(var key in result[i]){
          var metadata_field = _.find(metadata_result.fields, {name: key});

          if(!!metadata_field){
            if(metadata_field.eng_title == 'ISFOLDER'){
              output_obj["isfolder"] = result[i][key] == 1;
            }else if(metadata_field.eng_title == 'ID' || metadata_field.eng_title == 'PARENTID'){
              output_obj[metadata_field.eng_title] = result[i][key];
            }else{
              output_obj[metadata_field.eng_title] = typeof(result[i][key])=='string'? result[i][key].trim(): result[i][key];
            }


          }

        }
        output.push(output_obj);
      }

      console.log('Total items:',test_objects.length);
      console.log('Result:', output.length);
      callback(null, output);
    }else{
      callback('only Products in testing');
    }

  }
};
