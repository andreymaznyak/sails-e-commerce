/**
 * Created by MrHant on 30.03.2016.
 */
module.exports = {
  create: notPermitted,
  find: function(req,res){
    User.find().exec(function(err, users){
      res.json(users);
    });
  },
  findone: notPermitted,
  update: notPermitted,
  destroy: notPermitted,
  populate: notPermitted,
  add: notPermitted,
  remove: notPermitted
};

function notPermitted(req, res){
  res.forbidden('You are not permitted to perform user action.');
}
